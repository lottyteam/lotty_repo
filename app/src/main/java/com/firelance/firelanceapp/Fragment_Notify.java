package com.firelance.firelanceapp;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Created by PIKKEEW on 23/1/2561.
 */

public class Fragment_Notify extends Fragment {

    ListView simpleList;
    String countryList[] = {"India", "China", "australia", "Portugle", "America", "NewZealand"};
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notify,null);

        simpleList = (ListView) view.findViewById(R.id.mainListView);
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.row_notify, R.id.Notify_Title, countryList);
        simpleList.setAdapter(arrayAdapter);

        return view;
    }
}

package com.firelance.firelanceapp.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by user on 7/3/2561.
 */

@IgnoreExtraProperties
public class Post {
    public String uid;
    public String title;
    public String detail;
    public String cost;
    public String period;

    public Post() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public Post(String uid, String title, String body,String cost,String period) {
        this.uid = uid;
        this.title = title;
        this.detail = body;
        this.cost = cost;
        this.period = period;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("title", title);
        result.put("detail", detail);
        result.put("cost", cost);
        result.put("period",period);
        return result;
    }
}
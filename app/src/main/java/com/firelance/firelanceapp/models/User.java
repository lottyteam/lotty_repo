package com.firelance.firelanceapp.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;
/**
 * Created by user on 7/3/2561.
 */

@IgnoreExtraProperties
public class User {
    public String address;
    public String email;
    public String firstname;
    public String lastname;
    public Double score = 0.0;
    public String tel;
    public String title;

    public User() {
        // Default constructor required for calls to DataSnapshot.getValue(Post.class)
    }

    public User(String address, String email, String firstname,String lastname,String tel,String title) {
        this.email = email;
        this.title = title;
        this.address = address;
        this.firstname = firstname;
        this.lastname = lastname;
        this.tel = tel;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("email", email);
        result.put("title", title);
        result.put("address", address);
        result.put("score",score);
        result.put("firstname", firstname);
        result.put("lastname",lastname);
        result.put("tel",tel);
        return result;
    }
}
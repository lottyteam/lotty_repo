package com.firelance.firelanceapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Fragment_Profile_Edit extends Fragment {
    EditText firstname,lastname,address_display,email_display,tel_display;
    Button btn_edit;

    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
    DatabaseReference edit = FirebaseDatabase.getInstance().getReference();
    DatabaseReference edit2 = edit.child("profile");
    DatabaseReference edit3 = edit2.child(currentFirebaseUser.getUid());


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_edit, null);

        btn_edit = (Button) view.findViewById(R.id.btn_edit);
        firstname = (EditText) view.findViewById(R.id.firstname);
        lastname = (EditText) view.findViewById(R.id.lastname);
        address_display = (EditText) view.findViewById(R.id.address);
        email_display = (EditText) view.findViewById(R.id.email);
        tel_display = (EditText) view.findViewById(R.id.tel);
        //currentFirebaseUser.getUid()
        /*FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mProfileRef = mRootRef.child("profile");
        DatabaseReference mUidRef = mProfileRef.child(currentFirebaseUser.getUid());


        mUidRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String first_name = dataSnapshot.child("firstname").getValue(String.class);
                String last_name = dataSnapshot.child("lastname").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String email = dataSnapshot.child("email").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);
                firstname.setText(first_name);
                lastname.setText(last_name);
                address_display.setText(address);
                email_display.setText(email);
                tel_display.setText(tel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });*/

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit3.child("firstname").setValue(firstname.getText().toString());
                edit3.child("lastname").setValue(lastname.getText().toString());
                edit3.child("address").setValue(address_display.getText().toString());
                edit3.child("email").setValue(email_display.getText().toString());
                edit3.child("tel").setValue(tel_display.getText().toString());

                Toast.makeText(getActivity(), "Update Successful", Toast.LENGTH_LONG).show();

            }

        });


        return view;
    }
    

}

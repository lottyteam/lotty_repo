package com.firelance.firelanceapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

/**
 * Created by PIKKEEW on 23/1/2561.
 */

public class Fragment_Profile extends Fragment implements View.OnClickListener {
    TextView full_name,score_display,email_display,tel_display,address_display;
    private Button btn_edit;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile,null);

        String uid = getArguments().getString("uid");

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        String myuid = currentFirebaseUser.getUid();

        full_name = (TextView) view.findViewById(R.id.Profile_name);
        score_display = (TextView) view.findViewById(R.id.Profile_review);
        email_display = (TextView) view.findViewById(R.id.Profile_email);
        tel_display = (TextView) view.findViewById(R.id.Profile_tel);
        address_display = (TextView) view.findViewById(R.id.Profile_address);
        btn_edit= (Button)view.findViewById(R.id.btn_edit);


        if(myuid == uid){
            btn_edit.setVisibility(view.VISIBLE);
            btn_edit.setOnClickListener(this);
        }else
            btn_edit.setVisibility(view.INVISIBLE);

        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mProfileRef = mRootRef.child("profile");
        //DatabaseReference mUidRef = mProfileRef.child(currentFirebaseUser.getUid());
        DatabaseReference mUidRef = mProfileRef.child(uid);
        mUidRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String title = dataSnapshot.child("title").getValue(String.class);
                String first_name = dataSnapshot.child("firstname").getValue(String.class);
                String last_name = dataSnapshot.child("lastname").getValue(String.class);
                Double score = dataSnapshot.child("score").getValue(Double.class);
                String email = dataSnapshot.child("email").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);
                full_name.setText(title+ " " + first_name + " " + last_name);
                score_display.setText("คะแนน : " + score + " คะแนน" );
                email_display.setText("อีเมลล์ : " + email);
                address_display.setText("ที่อยู่ : " + address);
                tel_display.setText("เบอร์โทรศัพท์ : "+ tel);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
        return view;
    }
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_edit:
                changefragment(new Fragment_Profile_Edit());
                break;
        }

    }
    //Change Fragment
    public void changefragment(Fragment fragment){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.screen_area,fragment);
        ft.addToBackStack(null);
        ft.commit();
    }
}

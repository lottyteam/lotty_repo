package com.firelance.firelanceapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    TextView textNotiCount;
    int NotiCount = 8;
    //fragment default
    Fragment fragment = new Fragment_Home();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //Change to home
        changefragment(fragment);

        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
        DatabaseReference mRootRef = FirebaseDatabase.getInstance().getReference();
        DatabaseReference mProfileRef = mRootRef.child("profile");
        DatabaseReference mUidRef = mProfileRef.child(currentFirebaseUser.getUid());

        mUidRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String title = dataSnapshot.child("title").getValue(String.class);
                String first_name = dataSnapshot.child("firstname").getValue(String.class);
                String last_name = dataSnapshot.child("lastname").getValue(String.class);
                Double score = dataSnapshot.child("score").getValue(Double.class);
                String email = dataSnapshot.child("email").getValue(String.class);
                String address = dataSnapshot.child("address").getValue(String.class);
                String tel = dataSnapshot.child("tel").getValue(String.class);

                NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
                View headerView = navigationView.getHeaderView(0);
                TextView navUsername = (TextView) headerView.findViewById(R.id.nav_profile_name);

                navUsername.setText(title+" "+first_name+" "+last_name);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        //badge
        final MenuItem menuItem = menu.findItem(R.id.action_notify);

        View actionView = menuItem.getActionView();
        textNotiCount = (TextView) actionView.findViewById(R.id.cart_badge);

        setupBadge();

        actionView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(menuItem);
            }
        });
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        Fragment fragment = null;

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_notify) {
            fragment = new Fragment_Notify();
            changefragment(fragment);
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_find) {
            fragment = new Fragment_Search();
            changefragment(fragment);
        } else if (id == R.id.nav_hire) {
            fragment = new Fragment_Hire();
            changefragment(fragment);
        } else if (id == R.id.nav_assign) {
            fragment = new Fragment_Work();
            changefragment(fragment);
            changefragment(fragment);
        } else if (id == R.id.nav_profile) {
            FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
            String uid = currentFirebaseUser.getUid();
            fragment = new Fragment_Profile();
            fragmentProfile(fragment,uid);
        } else if (id == R.id.nav_home){
            fragment = new Fragment_Home();
            changefragment(fragment);
        } else if (id == R.id.nav_logout){
            FirebaseAuth.getInstance().signOut();
            Intent i = new Intent(getApplicationContext(), Login.class);
            startActivity(i);
            finish();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    private void setupBadge() {

        if (textNotiCount != null) {
            if (NotiCount == 0) {
                if (textNotiCount.getVisibility() != View.GONE) {
                    textNotiCount.setVisibility(View.GONE);
                }
            } else {
                textNotiCount.setText(String.valueOf(Math.min(NotiCount, 99)));
                if (textNotiCount.getVisibility() != View.VISIBLE) {
                    textNotiCount.setVisibility(View.VISIBLE);
                }
            }
        }
    }
    //Change Fragment
    public void changefragment(Fragment fragment){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.screen_area,fragment);
        ft.commit();
    }

    public void fragmentProfile(Fragment fragment,String  uid){
        Bundle bundle = new Bundle();
        bundle.putString("uid", uid);
        fragment.setArguments(bundle);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.screen_area,fragment);
        ft.commit();
    }
}

package com.firelance.firelanceapp;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firelance.firelanceapp.models.Post;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;


public class Fragment_Hire extends Fragment {

    EditText workname , detail  , period ,cost ;
    Button confirm ;

    FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;
    DatabaseReference postwork = FirebaseDatabase.getInstance().getReference();
    DatabaseReference postwork2 = postwork.child("Postwork");


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_hire,null);

        workname = (EditText)view.findViewById(R.id.workname);
        detail = (EditText)view.findViewById(R.id.detail);
        period = (EditText)view.findViewById(R.id.period);
        cost = (EditText)view.findViewById(R.id.cost);
        confirm = (Button) view.findViewById(R.id.confirm);


        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                writeNewPost(currentFirebaseUser.getUid(), workname.getText().toString(), detail.getText().toString()
                        ,cost.getText().toString(),period.getText().toString());
                Toast.makeText(getActivity(), "Add Document Successful", Toast.LENGTH_LONG).show();
            }
        });

        return view;
    }
    public void writeNewPost(String userId, String title, String body, String cost, String period) {

        String key = postwork2.push().getKey();
        Post post = new Post(userId, title, body, cost, period);
        Map<String, Object> postValues = post.toMap();

        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put(key, postValues);
        postwork2.updateChildren(childUpdates);
    }
}

